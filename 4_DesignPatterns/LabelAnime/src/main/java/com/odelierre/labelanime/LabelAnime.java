package com.odelierre.labelanime;

import javax.swing.JLabel;

public class LabelAnime extends Animateur {
    private JLabel label;
    
    public LabelAnime(JLabel label) {
        this.label = label;
    }
    
    public void animer() {}
    
    public Object getObject() {
        return this.label;
    }
}
