package com.odelierre.labelanime;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

public class AnimationTourner extends Decorateur {
    public AnimationTourner(Animateur animateur) {
        super(animateur);
    }
    
    public void animer() {
        super.animer();
        JLabel label = (JLabel)this.getObject();
        new Thread(new Runnable() {

            public void run() {
                while (true) {
                    label.setText(label.getText().substring(1) + label.getText().substring(0, 1));
                    try {
                        Thread.sleep(200);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(AnimationTourner.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        }).start();
    }
}
