package com.odelierre.labelanime;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;

public class AnimationClignoter extends Decorateur {
    public AnimationClignoter(Animateur animateur) {
        super(animateur);
    }
    
    public void animer() {
        super.animer();
        JLabel label = (JLabel)this.getObject();
        new Thread(new Runnable() {

            public void run() {
                Color couleur = label.getForeground();
                while(true) {
                    if (label.getForeground() == couleur) {
                        label.setForeground(label.getBackground());
                        try {
                            Thread.sleep(200);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(AnimationClignoter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } else {
                        label.setForeground(couleur);
                        try {
                            Thread.sleep(500);
                        } catch (InterruptedException ex) {
                            Logger.getLogger(AnimationClignoter.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                    }
                    
                }
            }
        }).start();
    }
}
