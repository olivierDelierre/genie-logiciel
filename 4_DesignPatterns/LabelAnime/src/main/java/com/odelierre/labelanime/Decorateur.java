package com.odelierre.labelanime;

public abstract class Decorateur extends Animateur {
    Animateur animateur;
    
    public Decorateur(Animateur animateur) {
        this.animateur = animateur;
    }
    
    public void animer() {
        animateur.animer();
    }
    
    public Object getObject() {
        return animateur.getObject();
    }
}
