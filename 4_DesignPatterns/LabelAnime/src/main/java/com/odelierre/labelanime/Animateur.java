package com.odelierre.labelanime;

public abstract class Animateur {
    public abstract void animer();
    public abstract Object getObject();
}
