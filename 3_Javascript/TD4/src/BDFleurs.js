const fs = require('fs');
const path = require('path');
const util = require('util');
const readFile = util.promisify(fs.readFile);
const access = util.promisify(fs.access);


class BaseDonneesFleurs {
  constructor(nomFichier) {
    this._fichierFleurs = nomFichier;
  }
  
  significationsDeLaFleur(nomFleur) {
    return readFile(this._fichierFleurs,'utf-8')
      .then(data => {
        const fleurs = JSON.parse(data);
        const fleur = fleurs.find(f => f.nom === nomFleur);
        return fleur ? fleur.signification : new Error(`fleur ${nomFleur} non trouvée`);
      });
  }
  
  fleursAyantLaSignification(signification) {
    return readFile(this._fichierFleurs,'utf-8')
      .then(data => {
        const fleurs = JSON.parse(data);
        return fleurs.filter(fleur => fleur.signification.indexOf(signification)!==-1)
                     .map(fleur => fleur.nom);
      });
  }
}

function creerBaseFleurs (nomFichier) {
  if (path.extname(nomFichier) !== '.json') {
      nomFichier += '.json';
  }
  nomFichier = path.resolve(nomFichier);
  return access(nomFichier, fs.constants.R_OK)
           .then(() => new BaseDonneesFleurs(nomFichier));
}

module.exports = creerBaseFleurs;