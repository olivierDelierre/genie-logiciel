function monteSite(app) {
    app.use('/site', require('./site'));
    app.use('/api', require('./api'));
}

module.exports = monteSite;