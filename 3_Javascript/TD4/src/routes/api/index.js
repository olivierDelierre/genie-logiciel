const router = require('express').Router();

router.get('/', (req, res) => {
    res.send('API index');
});

router.use('/fleurs', require('./fleurs'));

module.exports = router;