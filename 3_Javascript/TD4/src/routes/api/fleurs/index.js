const router = require('express').Router();

var dbPromise = require('../../../BDFleurs')('flowers.json');

router.get('/', (req, res) => {
    res.send('API fleurs');
})

router.get('/significations', (req, res) => {
    let nomFleur = req.query.nomfleur;
    let type = req.query.type;
    
    if (type === 'json') {
        dbPromise.then(db => {
            db.significationsDeLaFleur(nomFleur).then(result => {
                res.send(result);
            })
        })
    } else {
        res.send('Significations!');
    }
});

module.exports = router;