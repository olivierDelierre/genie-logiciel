const express = require('express');
const router = express.Router();

router.use(express.static('public_html'));
router.use((req, res) => {
    res.sendFile(__dirname + '/public_html/index.html');
});

module.exports = router;