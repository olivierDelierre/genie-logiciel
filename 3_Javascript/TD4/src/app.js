var express = require('express');
var app = express();

require('./routes')(app);

app.listen('3000', () => {
    console.log('Server listening on port 3000.');
});